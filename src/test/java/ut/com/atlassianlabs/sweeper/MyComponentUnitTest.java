package ut.com.atlassianlabs.sweeper;

import org.junit.Test;
import com.atlassianlabs.sweeper.MyPluginComponent;
import com.atlassianlabs.sweeper.MyPluginComponentImpl;

import static org.junit.Assert.assertEquals;

public class MyComponentUnitTest
{
    @Test
    public void testMyName()
    {
        MyPluginComponent component = new MyPluginComponentImpl(null);
        assertEquals("names do not match!", "myComponent",component.getName());
    }
}