package com.atlassianlabs.sweeper;

public interface MyPluginComponent
{
    String getName();
}