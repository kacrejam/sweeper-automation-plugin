package com.atlassianlabs.sweeper;

import static com.atlassian.plugin.automation.util.ParameterUtil.singleValue;

import java.util.Calendar;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import com.atlassian.crowd.embedded.api.User;
import com.atlassian.jira.bc.issue.IssueService;
import com.atlassian.jira.bc.issue.IssueService.DeleteValidationResult;
import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.user.ApplicationUsers;
import com.atlassian.jira.user.util.UserManager;
import com.atlassian.plugin.automation.core.Action;
import com.atlassian.plugin.automation.core.action.ActionConfiguration;
import com.atlassian.plugin.automation.core.auditlog.AuditString;
import com.atlassian.plugin.automation.core.auditlog.DefaultAuditString;
import com.atlassian.plugin.automation.util.ErrorCollection;
import com.atlassian.plugin.automation.util.ParameterUtil;
import com.atlassian.sal.api.message.I18nResolver;
import com.atlassian.soy.renderer.SoyException;
import com.atlassian.soy.renderer.SoyTemplateRenderer;
import com.google.common.collect.Maps;
import org.joda.time.*;

/**
 * Implements Deleting old issues
 */
public class DeleteOldIssuesAction implements Action<Issue>
{
    // These keys are bound to the field names from SOY template
    public static final String DELETE_OLD_ISSUES_VALUE_KEY = "deleteOldIssuesValueField";

    private static final Logger log = Logger.getLogger(DeleteOldIssuesAction.class);
    private static final String RESOURCE_KEY = "com.atlassianlabs.sweeper.automation-plugin-sweeper:automation-action-resources";
    private final SoyTemplateRenderer soyTemplateRenderer;
    private String issuesOlderThan;
    private int affectedIssues;

    public DeleteOldIssuesAction(final SoyTemplateRenderer soyTemplateRenderer)
    {
        this.soyTemplateRenderer = soyTemplateRenderer;
    }

    @Override
    public void init(ActionConfiguration config)
    {
        // This method is used to extract parameters provided when configuring the form for actual execution
        issuesOlderThan = singleValue(config, DELETE_OLD_ISSUES_VALUE_KEY);
        affectedIssues = 0;
    }

    @Override
    public void execute(String actor, Iterable<Issue> items, ErrorCollection errorCollection)
    {
    	int daysElapsed, issuesOlderThan_int;
    	DateTime lastUpdated, currentTime;
    	Calendar calendar;
    	UserManager userManager;
    	User user;
    	IssueService issueService;
    	com.atlassian.jira.util.ErrorCollection deletionErrors;
    	
    	// Get the user specified by actor username parameter
    	userManager = ComponentAccessor.getUserManager();
    	issueService = ComponentAccessor.getIssueService();
    	user = ApplicationUsers.toDirectoryUser(userManager.getUserByName(actor));
    	
    	// Get integer representation of specified number of days
    	issuesOlderThan_int = Integer.parseInt(issuesOlderThan);
    	
        // All issues returned by a trigger will be passed to this method. Use errorCollection to return any errors to the caller
        for (Issue issue : items)
        {
            // Get the time when the issue was updated and the current time
        	lastUpdated = new DateTime(issue.getUpdated());
        	calendar = Calendar.getInstance();
        	currentTime = new DateTime(calendar.getTime());
        	
        	// Get the number of days elapsed between current time and last time when issue was updated
        	calendar.setTime(lastUpdated.toDate());
        	daysElapsed = Days.daysBetween(lastUpdated, currentTime).getDays();
        	
        	// Try if the user is eligible to delete this issue
        	DeleteValidationResult deleteValidationResult = issueService.validateDelete(user, issue.getId());
        	if (deleteValidationResult.isValid()) 
        	{
        		if (daysElapsed > issuesOlderThan_int)
        		{
        			// Delete this issue and check for errors in deletion
        		    deletionErrors = issueService.delete(user, deleteValidationResult);
        		    if (deletionErrors.hasAnyErrors())
        			    errorCollection.addError(DELETE_OLD_ISSUES_VALUE_KEY, 
        			    		"There was an error deleting issue, id = " + issue.getId());
        		    else 
                		affectedIssues++;
        		}    
        	}	
        	
        }
    }

    @Override
    public AuditString getAuditLog()
    {
        // This will be added as audit log line
        return new DefaultAuditString(String.format("Deleted %d issues older than %s days",
        		                                     affectedIssues, issuesOlderThan));
    }

    @Override
    public String getConfigurationTemplate(ActionConfiguration actionConfiguration, String actor)
    {
        // This method needs to return the rendered HTML fragment which will be used in the UI when configuring the action
        try
        {
            final Map<String, Object> context = Maps.newHashMap();
            ParameterUtil.transformParams(context, actionConfiguration);
            return soyTemplateRenderer.render(RESOURCE_KEY, "Atlassian.Sweeper.Templates.Automation.deleteOldIssues", context);
        }
        catch (SoyException e)
        {
            log.error("Error rendering template", e);
            return "Unable to render configuration form. Consult your server logs or administrator.";
        }
    }

    @Override
    public String getViewTemplate(ActionConfiguration actionConfiguration, String s)
    {
        // This method needs to return the rendered HTML fragment which will be used in the UI when viewing the action
        try
        {
            final Map<String, Object> context = Maps.newHashMap();
            ParameterUtil.transformParams(context, actionConfiguration);
            return soyTemplateRenderer.render(RESOURCE_KEY, "Atlassian.Sweeper.Templates.Automation.deleteOldIssuesView", context);
        }
        catch (SoyException e)
        {
            log.error("Error rendering template", e);
            return "Unable to render configuration form. Consult your server logs or administrator.";
        }
    }

    @Override
    public ErrorCollection validateAddConfiguration(I18nResolver i18n, Map<String, List<String>> params, String actor)
    {
        // This method will be called when parameters to the action should be validated
        final ErrorCollection errors = new ErrorCollection();
        
        if (!params.containsKey(DELETE_OLD_ISSUES_VALUE_KEY) || StringUtils.isBlank(singleValue(params, DELETE_OLD_ISSUES_VALUE_KEY)))
        {
            errors.addError(DELETE_OLD_ISSUES_VALUE_KEY, i18n.getText("Value cannot be empty"));
        }
        else 
        {
        	String inputValue = params.get(DELETE_OLD_ISSUES_VALUE_KEY).get(0);
        	if (!isNumeric(inputValue))
        		errors.addError(DELETE_OLD_ISSUES_VALUE_KEY, "Value has to contain numbers only");
        	else 
        	{
        	    int inputValueInt = Integer.parseInt(inputValue);
        	    if (inputValueInt<0)
                	errors.addError(DELETE_OLD_ISSUES_VALUE_KEY, "Value has to be positive");
            }
        }
        return errors;
    }
    
    
    
    
    private static boolean isNumeric(String str)  
    {  
    	  try  
    	  {  
    	    Integer.parseInt(str);  
    	  }  
    	  catch(NumberFormatException nfe)  
    	  {  
    	    return false;  
    	  }  
    	  return true;  
    }


}
